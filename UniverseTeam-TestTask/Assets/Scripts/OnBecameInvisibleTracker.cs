﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnBecameInvisibleTracker : MonoBehaviour
{
    public GameObject ListenerInstance;
    private IBecomingInvisible _listener;

    void Start()
    {
        _listener = ListenerInstance.GetComponent<IBecomingInvisible>();
    }
    void OnBecameInvisible()
    {
        _listener.OnObjectBecamInvisible();
    }
}
