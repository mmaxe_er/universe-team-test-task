﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct PoolObjectSetup
{
    public GameObject Prefab;
    public int InitialCount;
}

public class Pool : MonoBehaviour
{
    [SerializeField]
    private Transform _instanceFolder;
    [SerializeField]
    private PoolObjectSetup[] _poolObjectSettings;

    // second dictionary contains data if instance is busy
    private Dictionary<string, Dictionary<PoolingObject, bool>> _instanceByName;

    public void Init()
    {
        _instanceByName = new Dictionary<string, Dictionary<PoolingObject, bool>>();
        
        foreach(var setup in _poolObjectSettings)
        {
            // instantiate prefabs
            var prefab = setup.Prefab;
            //var prefabName = prefab.name;
            var count = setup.InitialCount;
            for (int i = 0; i < count; i++)
            {
                CreateInstance(prefab, false);
            }
        }

        Pooling.SetPool(this);
    }

    private GameObject CreateInstance(GameObject prefab, bool isBusy)
    {
        // create and cache the instance
        var prefabName = prefab.name;
        var instance = Object.Instantiate(prefab, _instanceFolder);
        var poolingObject = instance.GetComponent<PoolingObject>();
        if (poolingObject == null)
        {
            poolingObject = instance.AddComponent<PoolingObject>();
        }
        poolingObject.SetName(prefabName);
        if (!_instanceByName.ContainsKey(prefabName))
        {
            _instanceByName[prefabName] = new Dictionary<PoolingObject, bool>();
        }
        _instanceByName[prefabName][poolingObject] = isBusy;
        instance.SetActive(isBusy);

        return instance;
    }

    public GameObject Instantiate(GameObject prefab, Vector3 position, Quaternion rotation, Transform parent)
    {
        GameObject instance = null;
        var prefabName = prefab.name;
        if (_instanceByName.ContainsKey(prefabName))
        {
            var instances = _instanceByName[prefabName];
            if (instances.ContainsValue(false))
            {
                foreach(var kvp in instances)
                {
                    // if instance is not busy, return it
                    if (kvp.Value == false)
                    {
                        // Debug.LogFormat("Found instance of prefab {0}", prefab.name);
                        instance = kvp.Key.gameObject;
                        instances[kvp.Key] = true;
                        break;
                    }
                }
            }
        }

        // if no proper instance found, create new one
        if (instance == null)
        {
            instance = CreateInstance(prefab, true);
        }

        if (instance != null)
        {
            instance.transform.SetParent(parent);
            instance.transform.localPosition = position;
            instance.transform.localRotation = rotation;
            instance.SetActive(true);
        }

        return instance;
    }

    public void DestroyPoolingObject(PoolingObject instance)
    {
        var instanceName = instance.PoolingObjectName;
        
        if (_instanceByName.ContainsKey(instanceName))
        {
            try
            {
                // mark instance as not busy
                var instances = _instanceByName[instanceName];
                instances[instance] = false;
                if (instance.gameObject.activeSelf)
                {
                    instance.gameObject.SetActive(false);
                }
                instance.ObjectTransform.parent = _instanceFolder;
            }
            catch (System.Exception e)
            {
                Debug.LogFormat("Exception occured while returning object into pool: {0}", e.Message);
            }
        }
        else
        {
            Object.Destroy(instance.gameObject);
        }
    }
}
