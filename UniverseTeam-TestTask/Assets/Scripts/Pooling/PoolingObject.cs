﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PoolingObject : MonoBehaviour
{
    [SerializeField]
    private string _poolingObjectName;
    public string PoolingObjectName
    {
        get
        {
            return _poolingObjectName;
        }
    }

    private Transform _cachedTransform;
    public Transform ObjectTransform
    {
        get
        {
            if (_cachedTransform == null)
            {
                _cachedTransform = this.transform;
            }
            return _cachedTransform;
        }
    }
    private Rigidbody _cachedRB;
    private Rigidbody _rb
    {
        get
        {
            if (_cachedRB == null)
            {
                _cachedRB = GetComponent<Rigidbody>();
            }
            return _cachedRB;
        }
    }

    public void SetName(string name)
    {
        _poolingObjectName = name;
    }

    public void ReturnToPool()
    {
        ResetState();
        
        if (gameObject.activeSelf)
        {
            Pooling.DestroyPoolingObject(this);
        }
    }

    public void ReturnToPoolWithChildren()
    {
        var children = GetComponentsInChildren<PoolingObject>();
        foreach (var po in children)
        {
            if (po == this) continue;
            po.ReturnToPool();
        }

        ReturnToPool();
    }

    private void ResetState()
    {
        if (_rb != null)
        {
            _rb.velocity = Vector3.zero;
            _rb.angularVelocity = Vector3.zero;
        }
    }
}
