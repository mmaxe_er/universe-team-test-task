﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Pooling
{
    private static Pool _pool;
    public static void SetPool(Pool pool)
    {
        _pool = pool;
    }

    public static GameObject Instantiate
    (
        GameObject prefab, 
        Vector3 position, 
        Quaternion rotation,
        Transform parent = null
    )
    {
        return _pool.Instantiate
        (
            prefab,
            position,
            rotation,
            parent
        );
    }

    public static void DestroyPoolingObject(PoolingObject poolingObject)
    {
        // Debug.LogFormat("Destroying instance {0}", poolingObject.name);
        _pool.DestroyPoolingObject(poolingObject);
    }
}
