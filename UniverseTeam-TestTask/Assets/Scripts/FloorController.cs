﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum TagTypes
{
    None,
    Enemy,
    Friend,
    LevelEnd
}


[System.Serializable]
public struct ObstacleSetup
{
    public string PrefabName;
    public TagTypes TagType;
    public Vector3 LocalPosition;
    public Quaternion LocalRotation;
}

[RequireComponent(typeof(PoolingObject))]
// TODO: split into MVC
public class FloorController : MonoBehaviour, IBecomingInvisible
{
    [Header("Graphics")]
    [SerializeField]
    private Renderer _floorRenderer;
    [SerializeField]
    private Renderer[] _edgeRenderers;

    [Header("Obstacles")]
    [SerializeField]
    private List<GameObject> _obstaclePrefabs;
    private Dictionary<string, GameObject> _obstaclePrefabByName;
    [SerializeField]
    private Transform _obstacleFolder;
    [SerializeField]
    private List<ObstacleSetup> _obstacleSettings;

    private PoolingObject _poolingObject;

    private Transform _cachedTransform;
    public Transform _transform 
    { 
        get
        {
            if (_cachedTransform == null)
            {
                _cachedTransform = this.transform;
            }
            return _cachedTransform;
        }
    }

    public void Init(ColorSetup colorSetup)
    {
        _poolingObject = GetComponent<PoolingObject>();

        _obstaclePrefabByName = new Dictionary<string, GameObject>();
        foreach (var prefab in _obstaclePrefabs)
        {
            _obstaclePrefabByName[prefab.name] = prefab;
        }

        _floorRenderer.material.color = colorSetup.FloorColor;
        for (int i = 0; i < _edgeRenderers.Length; i++)
        {
            _edgeRenderers[i].material.color = colorSetup.FloorEdgeColor;
        }

        for (int i = 0; i < _obstacleSettings.Count; i++)
        {
            var obstacleSetting = _obstacleSettings[i];
            
            if (_obstaclePrefabByName.ContainsKey(obstacleSetting.PrefabName))
            {
                var prefab = _obstaclePrefabByName[obstacleSetting.PrefabName];
                var obstacleObj = Pooling.Instantiate
                (
                    prefab, 
                    obstacleSetting.LocalPosition, 
                    obstacleSetting.LocalRotation, 
                    _obstacleFolder
                );

                var obstacleController = obstacleObj.GetComponent<ObstacleController>();
                obstacleController.Setup(obstacleSetting.TagType, colorSetup);
            }
        }
    }

    public void Move(float speed)
    {
        _transform.Translate(Vector3.forward * speed);
    }


    public void OnObjectBecamInvisible()
    {
        DestroySelf();
    }

    public void DestroySelf()
    {
        _poolingObject.ReturnToPoolWithChildren();
    }

#if UNITY_EDITOR
    [ContextMenu("Prepare Obstacles")]
    void PrepareObstacles()
    {
        foreach(Transform obstacle in _obstacleFolder)
        {
            var oName = obstacle.gameObject.name;
            //System.Text.RegularExpressions.Regex pattern = new System.Text.RegularExpressions.Regex(" (*)");
            oName = System.Text.RegularExpressions.Regex.Replace(oName, @" \(.+\)", string.Empty);
            obstacle.gameObject.name = oName;
        }
    }

    [ContextMenu("Save Obstacles")]
    void SaveObstacles()
    {
        UnityEditor.Undo.RecordObject(this, "Save Obstacles");
        _obstacleSettings.Clear();
        PrepareObstacles();
        foreach(Transform obstacle in _obstacleFolder)
        {
            var setup = new ObstacleSetup
            {
                PrefabName = obstacle.gameObject.name,
                TagType = obstacle.gameObject.CompareTag(TagTypes.Enemy.ToString()) ? TagTypes.Enemy : TagTypes.Friend,
                LocalPosition = obstacle.localPosition,
                LocalRotation = obstacle.localRotation
            };
            _obstacleSettings.Add(setup);
        }
    }

    [ContextMenu("Clear Obstacles")]
    void ClearObstacles()
    {
        UnityEditor.Undo.RecordObject(_obstacleFolder, "Clear Obstacles");
        var count = _obstacleFolder.childCount;
        while (count > 0)
        {
            DestroyImmediate(_obstacleFolder.GetChild(0).gameObject);
            count--;
        }
    }

#endif
}
