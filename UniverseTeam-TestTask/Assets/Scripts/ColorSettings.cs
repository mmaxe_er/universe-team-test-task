﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ColorSetup
{
    public Color PlayerColor;
    public Color ObstacleColor;
    public Color BackgroundColor;
    public Color FloorColor;
    public Color FloorEdgeColor;
}

[CreateAssetMenu(fileName = "ColorSettings", menuName = "Custom/Create/Color Settings")]
public class ColorSettings : ScriptableObject
{
    public List<ColorSetup> ColorSetups;

    public ColorSetup GetRandomColorSetup()
    {
        int index = Random.Range(0, ColorSetups.Count);
        return ColorSetups[index];
    }
}
