﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class UIController : IUIController
{
    private IUIView _view;

    public event Action<bool> OnMusicToggledEvent;
    public event Action OnPlayClickedEvent;
    public UIController(bool model, IUIView view)
    {
        _view = view;
        _view.OnPlayClickedEvent += OnPlayClicked;
        _view.OnMusicToggledEvent += OnMusicToggled;
        _view.Init(model);
    }

    private void OnPlayClicked()
    {
        OnPlayClickedEvent?.Invoke();
    }

    private void OnMusicToggled(bool isMusicOn)
    {
        _view.SetupMusicButtons(isMusicOn);
        OnMusicToggledEvent?.Invoke(isMusicOn);
    }

    public void Reset()
    {
        _view.ShowMainScreen();
    }

    public void HideAll()
    {
        _view.HideAll();
    }

    public void ShowGameOverScreen()
    {
        _view.ShowGameOverScreen();
    }

    public void ShowLevelPassedScreen()
    {
        _view.ShowLevelPassedScreen();
    }
}
