﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIView : MonoBehaviour, IUIView
{
    [SerializeField]
    private CanvasController _menu;
    [SerializeField]
    private CanvasController _game;
    [Header("Main Screen")]
    [SerializeField]
    private CanvasController _mainScreen;
    [Header("Settings Screen")]
    [SerializeField]
    private CanvasController _settingsScreen;
    [SerializeField]
    private Button _musicOnButton;
    [SerializeField]
    private Button _musicOffButton;
    [Header("Game Over Screen")]
    [SerializeField]
    private CanvasController _gameOverScreen;
    [Header("Level Passed Screen")]
    [SerializeField]
    private CanvasController _levelPassedScreen;

    public event Action OnPlayClickedEvent;
    public event Action<bool> OnMusicToggledEvent;

    public void Init(bool isMusicOn)
    {
        SetupMusicButtons(isMusicOn);
        ShowMainScreen();
    }

    public void Play()
    {
        OnPlayClickedEvent?.Invoke();
    }

    public void ShowMainScreen()
    {
        _menu.Enabled = true;
        _game.Enabled = false;
        _mainScreen.Enabled = true;
        _settingsScreen.Enabled = false;
    }

    public void ShowSettingsScreen()
    {
        _menu.Enabled = true;
        _game.Enabled = false;
        _mainScreen.Enabled = false;
        _settingsScreen.Enabled = true;
    }

    public void ShowGameOverScreen()
    {
        _menu.Enabled = false;
        _game.Enabled = true;
        _gameOverScreen.Enabled = true;
        _levelPassedScreen.Enabled = false;
    }

    public void ShowLevelPassedScreen()
    {
        _menu.Enabled = false;
        _game.Enabled = true;
        _gameOverScreen.Enabled = false;
        _levelPassedScreen.Enabled = true;
    }

    public void HideAll()
    {
        _menu.Enabled = false;
        _game.Enabled = false;
        _mainScreen.Enabled = false;
        _settingsScreen.Enabled = false;
        _gameOverScreen.Enabled = false;
        _levelPassedScreen.Enabled = false;
    }

    public void SetMusic(bool isOn)
    {
        OnMusicToggledEvent?.Invoke(isOn);
    }

    public void SetupMusicButtons(bool isOn)
    {
        _musicOnButton.interactable = !isOn;
        _musicOffButton.interactable = isOn;
    }
}
