﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CanvasController : MonoBehaviour
{
    public Canvas canvas;
    public GraphicRaycaster raycaster;

    public bool Enabled
    {
        set
        {
            if (canvas != null)
            {
                canvas.enabled = value;
            }
            if (raycaster != null)
            {
                raycaster.enabled = value;
            }
        }
    }
}
