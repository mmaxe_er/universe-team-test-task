﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class AudioManager : MonoBehaviour
{
    private AudioSource _audio;
    public void Init(bool isMusicOn)
    {
        _audio = GetComponent<AudioSource>();
        ToggleMusic(isMusicOn);
        _audio.Play();
    }

    public void ToggleMusic(bool isMusicOn)
    {
        _audio.mute = !isMusicOn;
    }
}
