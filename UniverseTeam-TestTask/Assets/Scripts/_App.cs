﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class _App : MonoBehaviour
{
    [Header("Config")]
    [SerializeField]
    public int _levelLength = 4;
    [SerializeField]
    private float _speed = 3;
    private float _timeScale = 1;
    private float _timeScaleFadeTime = 3;
    [SerializeField]
    private ColorSettings _colorSettings;
    private Vector3 _floorStep = new Vector3(0, 0, 10);
    [SerializeField]
    private GameObject _emptyFloorPrefab;
    [SerializeField]
    private GameObject _finishFloorPrefab;
    [SerializeField]
    private GameObject[] _floorPrefabs;

    [Header("Player Settings")]
    [SerializeField]
    private LayerMask _raycastLayerMask;
    [SerializeField]
    private GameObject _playerViewPrefab;

    [Header("UI")]
    [SerializeField]
    private AudioManager _audioManager;

    [Header("UI")]
    [SerializeField]
    private GameObject _uiViewInstance;
    private IUIController _uiController;

    [Header("Other")]
    const string musicToggled_KEY = "musicToggled";
    private bool isMusicToggled
    {
        get
        {
            return PlayerPrefs.GetInt(musicToggled_KEY, 1) == 1;
        }
        set
        {
            PlayerPrefs.SetInt(musicToggled_KEY, value ? 1 : 0);
        }
    }
    [SerializeField]
    private Pool _pool;

    private IPlayerController _player;
    private List<FloorController> _floorControllers = new List<FloorController>();

    enum AppState
    {
        None,
        Menu,
        WaitingForStart,
        Playing,
        GameOver,
        LevelPassed
    }

    private AppState _appState;

    void Start()
    {
        _audioManager.Init(isMusicToggled);

        InitUI();

        _uiController.OnPlayClickedEvent += OnPlayClicked;
        _uiController.OnMusicToggledEvent += OnMusicToggled;

        // TODO: create player factory
        CreatePlayer();
        _player.OnGameOverEvent += OnGameOver;
        _player.OnLevelPassedEvent += OnLevelPassed;

        _pool.Init();

        _appState = AppState.Menu;
    }

    void OnDestroy()
    {
        _player.OnGameOverEvent -= OnGameOver;
        _player.OnLevelPassedEvent -= OnLevelPassed;

        _uiController.OnPlayClickedEvent -= OnPlayClicked;
        _uiController.OnMusicToggledEvent -= OnMusicToggled;
    }

    void Restart()
    {
        _uiController.Reset();
        _appState = AppState.Menu;
    }

    void InitUI()
    {
        var view = _uiViewInstance.GetComponent<IUIView>();
        _uiController = new UIController(isMusicToggled, view);
    }

    private void OnMusicToggled(bool isMusicOn)
    {
        isMusicToggled = isMusicOn;
        _audioManager.ToggleMusic(isMusicOn);
    }

    private void OnPlayClicked()
    {
        _player.Reset();

        _uiController.HideAll();

        GenerateLevel(_levelLength);

        _appState = AppState.WaitingForStart;
    }

    void CreatePlayer()
    {
        PlayerModel model = new PlayerModel
        {
            RaycastLayerMask = _raycastLayerMask,
            LerpSpeed = 20
        };

        var viewObj = Instantiate(_playerViewPrefab, Vector3.zero, Quaternion.identity);
        var view = viewObj.GetComponent<IPlayerView>();

        _player = new PlayerController(model, view);
    }

    private void MoveLevel()
    {
        _floorControllers.RemoveAll(item => !item.gameObject.activeSelf);

        if (_timeScale > 0)
        {
            foreach (var floor in _floorControllers)
            {
                floor.Move(-_speed * Time.deltaTime * _timeScale);
            }
        }
    }

    private void GenerateLevel(int length)
    {
        if (_floorControllers.Count > 0)
        {
            foreach (var floor in _floorControllers)
            {
                floor.DestroySelf();
            }
            _floorControllers.Clear();
        }

        var colorSetup = _colorSettings.GetRandomColorSetup();
        _player.SetColor(colorSetup.PlayerColor);

        var pos = Vector3.zero;
        // create 1 empty floor at the beginning and 4 at the end of the level
        for (int i = 0; i < length + 1 + 4; i++)
        {
            var floorPrefab = _emptyFloorPrefab;
            if (i == length + 1)
            {
                floorPrefab = _finishFloorPrefab;
            }
            else if (i > 0 && i <= length)
            {
                var index = Random.Range(1, _floorPrefabs.Length);
                floorPrefab = _floorPrefabs[index];
            }

            var floorObj = Pooling.Instantiate(floorPrefab, pos, Quaternion.identity);
            var floor = floorObj.GetComponent<FloorController>();
            floor.Init(colorSetup);

            _floorControllers.Add(floor);

            pos += _floorStep;
        }
    }

    void Update()
    {
        switch (_appState)
        {
            case AppState.WaitingForStart:
                if (Input.GetMouseButtonDown(0))
                {
                    _timeScale = 1;
                    _appState = AppState.Playing;
                }
                _player.Update(_timeScale, true);
                break;
            case AppState.GameOver:
            case AppState.LevelPassed:
                if (_timeScale > 0)
                {
                    _timeScale -= Time.deltaTime / _timeScaleFadeTime;
                }
                _player.Update(_timeScale, false);
                MoveLevel();
                break;
            case AppState.Playing:
                _player.Update(_timeScale, true);
                MoveLevel();
                break;
        }
    }

    private void OnGameOver()
    {
        if (_appState == AppState.Playing)
        {
            _uiController.ShowGameOverScreen();
            _appState = AppState.GameOver;
        }
    }
    private void OnLevelPassed()
    {
        if (_appState == AppState.Playing)
        {
            _uiController.ShowLevelPassedScreen();
            _appState = AppState.LevelPassed;
        }
    }
}
