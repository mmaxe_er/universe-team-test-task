using UnityEngine;

public struct PlayerModel
{
    public LayerMask RaycastLayerMask;
    public Vector3 InputOffset;
    public float LerpSpeed;
    public Vector3 TargetPos;

}