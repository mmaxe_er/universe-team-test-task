﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class PlayerView : MonoBehaviour, IPlayerView
{
    public event Action<Vector3> OnUserPressedEvent; 
    public event Action<Vector3> OnUserHoldEvent; 
    public event Action<Collision> OnCollisionEnterEvent;
    public event Action<Collider> OnTriggerEnterEvent;

    [SerializeField]
    private Renderer _renderer;

    public Transform GetTransform()
    {
        return this.transform;
    }
    public GameObject GetGameObject()
    {
        return this.gameObject;
    }

    public void OnUpdate()
    {
        if (Input.GetMouseButtonDown(0))
        {
            OnUserPressedEvent?.Invoke(Input.mousePosition);
        }
        if (Input.GetMouseButton(0))
        {
            OnUserHoldEvent?.Invoke(Input.mousePosition);
        }
    }

    public void SetColor(Color color)
    {
        _renderer.material.color = color;
    }

    private void OnCollisionEnter(Collision c)
    {
        // unnecessary collisions are filtered by properly setup Layer Collision Mask in ProjectSettings
        OnCollisionEnterEvent?.Invoke(c);
    }
    private void OnTriggerEnter(Collider c)
    {
        // unnecessary collisions are filtered by properly setup Layer Collision Mask in ProjectSettings
        OnTriggerEnterEvent?.Invoke(c);
    }
}
