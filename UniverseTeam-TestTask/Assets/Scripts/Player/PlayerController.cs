﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : IPlayerController
{
    public event Action OnGameOverEvent;
    public event Action OnLevelPassedEvent;

    private Camera _cam;
    private Transform _transform;

    private PlayerModel _model;
    private IPlayerView _view;

    private bool _isReadingInput = true;
    public PlayerController(PlayerModel model, IPlayerView view)
    {
        _model = model;
        _view = view;

        _view.OnUserPressedEvent += OnUserPressed;
        _view.OnUserHoldEvent += OnUserHold;
        _view.OnCollisionEnterEvent += OnCollisionEnter;
        _view.OnTriggerEnterEvent += OnTriggerEnter;

        _cam = Camera.main;
        _transform = view.GetTransform();
        _model.TargetPos = _transform.position;
    }

    private void OnUserPressed(Vector3 mousePos)
    {
        if (!_isReadingInput) return;
        RaycastHit hit;
        if (Raycast(mousePos, out hit))
        {
            _model.InputOffset = _transform.position - hit.point;
        }
    }
    private void OnUserHold(Vector3 mousePos)
    {
        if (!_isReadingInput) return;
        RaycastHit hit;
        if (Raycast(mousePos, out hit))
        {
            _model.TargetPos = hit.point + _model.InputOffset;
            var xPos = _model.TargetPos.x;
            if (Mathf.Abs(xPos) > 3)
            {
                xPos = 3 * Mathf.Sign(xPos);
            }
            _model.TargetPos.x = xPos;
        }
    }

    public void Update(float timeScale, bool readInput)
    {
        _isReadingInput = readInput;
        _view.OnUpdate();
        // TODO: change position with rigidbody's velocity to avoid object overlapping on high speed
        _transform.position = Vector3.Lerp(_transform.position, _model.TargetPos, _model.LerpSpeed * Time.deltaTime * timeScale);
    }

    private bool Raycast(Vector3 mousePos, out RaycastHit hitInfo)
    {
        var ray = _cam.ScreenPointToRay(mousePos);
        return Physics.Raycast(ray, out hitInfo, Mathf.Infinity, _model.RaycastLayerMask.value);
    }

    public void SetColor(Color color)
    {
        _view.SetColor(color);
    }

    private void OnCollisionEnter(Collision c)
    {
        // unnecessary collisions are filtered by properly setup Layer Collision Mask in ProjectSettings
        if (c.gameObject.CompareTag(TagTypes.Enemy.ToString()))
        {
            OnGameOverEvent?.Invoke();
        }
    }
    private void OnTriggerEnter(Collider c)
    {
        // unnecessary collisions are filtered by properly setup Layer Collision Mask in ProjectSettings
        if (c.gameObject.CompareTag(TagTypes.LevelEnd.ToString()))
        {
            OnLevelPassedEvent?.Invoke();
        }
    }

    public void Dispose()
    {
        UnityEngine.Object.Destroy(_view.GetGameObject());
    }

    public void Reset()
    {
        _transform.position = Vector3.zero;
        _model.TargetPos = _transform.position;
    }
}
