﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(PoolingObject))]
// TODO: split into MVC
public class ObstacleController : MonoBehaviour, IBecomingInvisible
{
    [SerializeField]
    private Renderer _renderer;
    
    private PoolingObject _poolingObject;
    private MaterialPropertyBlock _propertyBlock;

    void Start()
    {
        _poolingObject = GetComponent<PoolingObject>();
    }

    public void Setup(TagTypes tagType, ColorSetup colorSetup)
    {
        gameObject.tag = tagType.ToString();

        var color = tagType == TagTypes.Enemy ? colorSetup.ObstacleColor : colorSetup.PlayerColor;

        // use MaterialPropertyBlock to avoid overhead on color changing
        if (_propertyBlock == null)
        {
            _propertyBlock = new MaterialPropertyBlock();
        }
        _renderer.GetPropertyBlock(_propertyBlock);
        _propertyBlock.SetColor("_Color", color);
        _renderer.SetPropertyBlock(_propertyBlock);
    }

    public void OnObjectBecamInvisible()
    {
        _poolingObject.ReturnToPool();
    }
}
