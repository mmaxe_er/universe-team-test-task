﻿public interface IBecomingInvisible
{
    void OnObjectBecamInvisible();
}
