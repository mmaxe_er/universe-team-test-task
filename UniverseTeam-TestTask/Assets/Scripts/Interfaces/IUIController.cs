using System;

public interface IUIController
{
    event Action OnPlayClickedEvent;
    event Action<bool> OnMusicToggledEvent;
    void Reset();
    void HideAll();
    void ShowGameOverScreen();
    void ShowLevelPassedScreen();
}
