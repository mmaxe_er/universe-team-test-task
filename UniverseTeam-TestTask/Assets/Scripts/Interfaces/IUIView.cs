using System;

public interface IUIView
{
    event Action OnPlayClickedEvent;
    event Action<bool> OnMusicToggledEvent;
    void Init(bool isMusicOn);
    void ShowMainScreen();
    void ShowSettingsScreen();
    void ShowGameOverScreen();
    void ShowLevelPassedScreen();
    void HideAll();
    void SetupMusicButtons(bool isMusicOn);
}
