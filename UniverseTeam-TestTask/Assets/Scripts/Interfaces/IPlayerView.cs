using System;
using UnityEngine;

public interface IPlayerView
{
    event Action<Vector3> OnUserPressedEvent; 
    event Action<Vector3> OnUserHoldEvent; 
    event Action<Collision> OnCollisionEnterEvent; 
    event Action<Collider> OnTriggerEnterEvent; 

    Transform GetTransform();
    void OnUpdate();
    void SetColor(Color color);
    GameObject GetGameObject();
}
