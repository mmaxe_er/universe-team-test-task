using System;
using UnityEngine;

public interface IPlayerController
{
    event Action OnLevelPassedEvent;
    event Action OnGameOverEvent;
    void Update(float timeScale, bool readInput);
    void SetColor(Color color);
    void Reset();
}